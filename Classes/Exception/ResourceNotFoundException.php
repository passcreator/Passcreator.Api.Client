<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 21.11.17
 * Time: 11:01
 */

namespace Passcreator\Api\Client\Exception;

use Neos\Flow\Exception;

class ResourceNotFoundException extends Exception
{

}