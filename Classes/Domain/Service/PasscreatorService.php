<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 21.11.17
 * Time: 09:57
 */

namespace Passcreator\Api\Client\Domain\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\Request;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Http\Client\CurlEngine;
use Passcreator\Api\Client\Exception\PasscreatorException;
use Passcreator\Api\Client\Exception\UnauthorizedException;
use Passcreator\Api\Client\Exception\ResourceNotFoundException;

class PasscreatorService
{

    /**
     * @var CurlEngine
     * @Flow\Inject
     */
    protected $requestEngine;

    /**
     * @Flow\InjectConfiguration(package="Passcreator.Api.Client")
     * @var array
     */
    protected $settings;

    /**
     * Copy the given template ID.
     * Example payload:
     * {
     * "name": "Template copy",
     * "description": "This is a description",
     * "organizationName": "Dummy",
     * "foregroundColor": "#000000",
     * "backgroundColor": "#cccccc",
     * "labelColor": "#ffffff",
     * "urlToLogo": "https://www.passcreator.com/examplelogo.png",
     * "urlToBackground": "https://www.passcreator.com/examplebackground.png",
     * "urlToIcon": "https://www.passcreator.com/exampleicon.png",
     * "locations": [{
     *   "latitude": 48.0933826,
     *   "longitude": 11.6286617,
     *   "relevantText": "Welcome to the show!",
     *   "altitude": 200,
     *   "maxDistance": 300
     *  }]
     * }
     *
     * @param string $templateId
     * @param array  $content
     *
     * @return array
     */
    public function copyTemplate(string $templateId, array $content): array
    {
        return $this->post('pass-template/copy/' . $templateId, $content);
    }

    /**
     * @param string     $resource  The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be send to the API endpoint
     *
     * @return array
     */
    private function post(string $resource, array $arguments = null): array
    {
        return $this->makeRequest('POST', $resource, $arguments);
    }

    /**
     * @param string     $method    The HTTP method
     * @param string     $resource  The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be sent to the API endpoint
     *
     * @return array The decoded response
     */
    private function makeRequest(string $method, string $resource, array $arguments = null): array
    {
        $client = new Client();
        $uri = new Uri($this->settings['apiEndpoint'] . '/' . $resource);

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => $this->settings['apiKey']
        ];

        $body = null;

        if ($method === 'GET' && $arguments !== null) {
            $uri .= http_build_query($arguments);
        }

        if ($method !== 'GET' && $arguments !== null) {
            $body = json_encode($arguments);
        }

        $request = new Request($method, $uri, $headers, $body);

        $response = $client->send($request);

        $decodedBody = json_decode($response->getBody(), true);
        if ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
            return $decodedBody;
        }
        $errorMessage = $decodedBody['detail'] ?? $response->getStatusCode() . ' Unknown error.';
        if ($response->getStatusCode() === 404) {
            throw new ResourceNotFoundException($errorMessage, 1483538558);
        }

        if ($response->getStatusCode() === 401) {
            throw new UnauthorizedException($errorMessage, 1483538559);
        }

        throw new PasscreatorException('Unknown error. Status code: ' . $response->getStatusCode(), 1483538559);
    }

    /**
     * Example input:
     * {
     *  "name": "Test API",
     *  "passTemplateId": "",
     *  "type": 0,
     *  "scanMode": 1,
     *  "additionalProperties": [
     *    {
     *     "type": "boolean",
     *     "name": "Allowed to send marketing mails?"
     *    },
     *    {
     *     "type": "unicode",
     *     "name": "Tell us about you"
     *    },
     *    {
     *     "type": "double",
     *     "name": "Transaction value"
     *   }
     *  ]
     * }
     *
     * @param array $content
     *
     * @return array
     */
    public function createAppConfiguration(array $content = array()): array
    {
        return $this->post('appconfiguration', $content);
    }

    /**
     * @param string $templateId
     * @param string $name
     * @param array  $content
     *
     * @return array
     */
    public function createIframeIntegration(string $templateId, string $name, array $content = []): array
    {
        $content['name'] = $name;

        return $this->post('integration/iframe/' . $templateId, $content);
    }

    /**
     * @param string $templateId
     * @param array  $content
     * @param bool   $zapierStyle
     *
     * @return array
     */
    public function createPass(string $templateId, array $content = array(), bool $zapierStyle = false): array
    {
        return $this->post('pass?passtemplate=' . $templateId . ($zapierStyle ? '&zapierStyle=true' : ''), $content);
    }

    /**
     * @param $passId
     *
     * @return array
     */
    public function deletePass($passId): array
    {
        return $this->delete('pass/' . $passId);
    }

    /**
     * @param string $resource
     *
     * @return array
     */
    private function delete(string $resource): array
    {
        return $this->makeRequest('DELETE', $resource);
    }

    /**
     * @param string $templateId
     *
     * @return array
     */
    public function describeTemplate(string $templateId): array
    {
        return $this->get('v2/pass-template/' . $templateId . '/describe');
    }

    /**
     * @param string $resource The REST resource name (e.g. "lists")
     *
     * @return array
     */
    private function get(string $resource): array
    {
        return $this->makeRequest('GET', $resource);
    }

    /**
     * @param string $passId
     *
     * @return array
     */
    public function getPassInfo(string $passId): array
    {
        return $this->get('pass/' . $passId);
    }

    /**
     * @param string $templateId
     * @param bool   $zapierStyle
     *
     * @return array
     */
    public function getTemplateInfo(string $templateId, bool $zapierStyle = false): array
    {
        return $this->get('pass-template/' . $templateId . ($zapierStyle ? '?zapierStyle=true' : ''));
    }

    /**
     * @return array
     */
    public function getTemplates(): array
    {
        return $this->get('pass-template');
    }

    /**
     * @param string $passId
     * @param bool   $voided
     *
     * @return array
     */
    public function markPassVoided(string $passId, bool $voided = true): array
    {
        return $this->put('pass/' . $passId, array('voided' => $voided));
    }

    /**
     * @param string     $resource  The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be sent to the API endpoint
     *
     * @return array
     */
    private function put(string $resource, array $arguments = null): array
    {
        return $this->makeRequest('PUT', $resource, $arguments);
    }

    /**
     * @param string $templateId
     * @param array  $content
     *
     * @return array
     */
    public function patchTemplate(string $templateId, array $content = array()): array
    {
        return $this->patch('v2/pass-template/' . $templateId, $content);
    }

    /**
     * @param string     $resource  The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be sent to the API endpoint
     *
     * @return array
     */
    private function patch(string $resource, array $arguments = null): array
    {
        return $this->makeRequest('PATCH', $resource, $arguments);
    }

    /**
     * @param $link
     *
     * @return array
     */
    public function renewAppConfigurationLink($link): array
    {
        return $this->post('appconfigurationlink/renew?link=' . $link);
    }

    /**
     * @param string $searchString
     * @param string $templateId
     *
     * @return array
     */
    public function searchPasses(string $searchString, string $templateId): array
    {
        return $this->get('pass/search/' . $templateId . '/' . $searchString);
    }

    /**
     * @param array $content
     */
    public function subscribeRestHook(array $content = array()): array
    {
        return $this->post('hook/subscribe/', $content);
    }

    /**
     * @param string $passId
     * @param array  $content
     * @param bool   $zapierStyle
     *
     * @return array
     */
    public function updatePass(string $passId, array $content = array(), bool $zapierStyle = false): array
    {
        return $this->post('pass/' . $passId . ($zapierStyle ? '?zapierStyle=true' : ''), $content);
    }

    /**
     * @param array       $data
     * @param array|null  $identifiers
     * @param array|null  $query
     * @param string|null $segmentId
     *
     * @return array
     */
    public function updatePassBulkV3(
        array $data,
        ?array $identifiers = null,
        ?array $query = null,
        ?string $segmentId = null
    ): array {
        $content = ['data' => $data];

        if (!empty($identifiers)) {
            $content['filter']['identifiers'] = $identifiers;
        }

        if (!empty($query) && empty($identifiers)) {
            $content['filter']['query'] = $query;
        }

        if (!empty($segmentId) && empty($identifiers) && empty($query)) {
            $content['filter']['segmentId'] = $query;
        }

        return $this->patch('v3/pass/bulk', $content);
    }

    /**
     * @param string $templateId
     * @param array  $content
     *
     * @return array
     */
    public function updateTemplate(string $templateId, array $content): array
    {
        return $this->post('pass-template/' . $templateId, $content);
    }

    /**
     * @param string $templateId
     * @param array  $content
     *
     * @return array
     */
    public function updateTemplateV2(string $templateId, array $content = array()): array
    {
        return $this->post('v2/pass-template/' . $templateId, $content);
    }

}
